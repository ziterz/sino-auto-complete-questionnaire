# SINO Auto Complete Questionnaire

![Semantic description of image](/images/title.png "Image Title")

Auto Complete Questionnaire for SINO (Sistem Informasi Nilai Online) - Universitas Pendidikan Indonesia

## How to use
- Buka file [**SINO.js**][file]
- Copy semua **script**
- Buka halaman quisioner per mata kuliah
- Tekan **F12** pada browser kalian (Buka Inspect Element)
- Pilih tab **console**
- Paste **script** diatas
- Tekan **enter**

[file]: https://gitlab.com/ziterz/sino-auto-complete-questionnaire/blob/master/SINO.js